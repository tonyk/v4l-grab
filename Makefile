all:
	gcc grab.c -o grab
all_debug:
	gcc grab.c -g -D DBG_PRINT -o grab	

run: all
	./grab
run_debug: all_debug
	./grab

run_trace:
	strace -o grab.trace ./grab

show_image:
	gst-launch-1.0 filesrc location=out000.yuv ! rawvideoparse use-sink-caps=false width=640 height=360 format=yv12 ! imagefreeze ! videoconvert ! ximagesink

clean:
	rm grab out000.yuv grab.trace
