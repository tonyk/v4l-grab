# v4l-grab

This code grabs a frame from vivid driver using V4L2 API for studying proposes.

Before run, make sure you are using vivid with the multi-planar API set, using
this kernel parameter: `vivid.multiplanar=2`.

After the run, the program will create a `out000.yuv` file with color bars. The
V4L2 pixel format is YVU420M

The Makefile contains compiling, run and testing instructions:

- all: normal compilation
- all_debug: compiles with debug prints

- run: normal compile and run
- run_debug: compile and run with debug prints

For bellow options, make sure you have run at least one of the command above:

- run_trace: run and generates a strace output file (grab.trace)
- show_image: uses gstreamer to show the frame in `out000.yuv`
- clean: remove the strace file, image and the binary
