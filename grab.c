/* V4L2 video picture grabber
   Copyright (C) 2009 Mauro Carvalho Chehab <mchehab@kernel.org>
   Copyright (C) 2018 Andre Almeida <andre.almeida@collabora.com> 

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <linux/videodev2.h>

// clear memory
#define CLEAR(x) memset(&(x), 0, sizeof(x))

// num of planes of YVU420M
#define NUM_PLANES 3

// plane buffer info
struct buffer {
	void   *start[NUM_PLANES]; 
	size_t length[NUM_PLANES];
};

// common data about the image
struct context {
	__u32 pix_format;
	__u32 buf_type;
	__u32 mem_type;
	__u8  num_planes;
	__u8  num_buffers;
	__u16 img_height;
	__u16 img_width;
	__u8  size_image;
};

static void xioctl(int fh, int request, void *arg)
{
        int r;

        do {
                r = ioctl(fh, request, arg);
        } while (r == -1 && ((errno == EINTR) || (errno == EAGAIN)));

        if (r == -1) {
                fprintf(stderr, "error %d, %s\n", errno, strerror(errno));
                exit(EXIT_FAILURE);
        }
}

/* query que capabilities of the driver */
void v4l2_querycap(int fd){
	struct v4l2_capability	caps;

	xioctl(fd, VIDIOC_QUERYCAP, &caps);

	/* check if the driver can operate with this app */
	if((caps.capabilities & V4L2_CAP_VIDEO_CAPTURE_MPLANE) == 0){
		perror("Device can't make a multi-planar video capture");
		exit(EXIT_FAILURE);
	}
	if((caps.capabilities & V4L2_CAP_STREAMING) == 0){
		perror("Device don't support streaming");
		exit(EXIT_FAILURE);
	}
}

/* negotiates a format with the driver */
void v4l2_s_fmt(int fd, struct context ctx){
	struct v4l2_format fmt;

	CLEAR(fmt);
	fmt.type 			= ctx.buf_type;
	fmt.fmt.pix_mp.width 		= ctx.img_width;
	fmt.fmt.pix_mp.height		= ctx.img_height;
	fmt.fmt.pix_mp.pixelformat	= ctx.pix_format;
	fmt.fmt.pix_mp.num_planes	= ctx.num_planes;

	/* doesn't make difference, driver overwrite this data */
	fmt.fmt.pix_mp.plane_fmt[0].sizeimage = 230400;
	fmt.fmt.pix_mp.plane_fmt[1].sizeimage = 57600;
	fmt.fmt.pix_mp.plane_fmt[2].sizeimage = 57600;

	xioctl(fd, VIDIOC_S_FMT, &fmt);

}

/* just try to set a format, but don't actually change the driver */
void v4l2_try_fmt(int fd, struct context ctx){
	struct v4l2_format fmt;

	CLEAR(fmt);
	fmt.type 			= ctx.buf_type;
	fmt.fmt.pix_mp.width 		= ctx.img_width;
	fmt.fmt.pix_mp.height		= ctx.img_height;
	fmt.fmt.pix_mp.pixelformat	= ctx.pix_format;
	fmt.fmt.pix_mp.num_planes	= ctx.num_planes;

	fmt.fmt.pix_mp.plane_fmt[0].sizeimage = 230400;
	fmt.fmt.pix_mp.plane_fmt[1].sizeimage = 57600;
	fmt.fmt.pix_mp.plane_fmt[2].sizeimage = 57600;

	xioctl(fd, VIDIOC_TRY_FMT, &fmt);

	__u8 num_planes = fmt.fmt.pix_mp.num_planes;
	printf("Number of planes: %d\n", num_planes);
	for(int i = 0; i < num_planes; i++)
		printf("plane[%d].sizeimage = %u\n", i,
			fmt.fmt.pix_mp.plane_fmt[i].sizeimage);

}

/* get actual driver format */
void v4l2_g_fmt(int fd, struct context ctx){
	struct v4l2_format fmt;

	xioctl(fd, VIDIOC_G_FMT, &fmt);

	/* Comparing fmt accepted by the driver */
	if (fmt.fmt.pix.pixelformat != ctx.pix_format) {
		printf("Device didn't accept YUV420M format (got %x). Can't proceed.\n",
			fmt.fmt.pix.pixelformat);
		exit(EXIT_FAILURE);
	}
	if ((fmt.fmt.pix.width != ctx.img_width) || (fmt.fmt.pix.height != ctx.img_height))
		printf("Warning: driver is sending image at %dx%d\n",
			fmt.fmt.pix.width, fmt.fmt.pix.height);


	__u8 num_planes = fmt.fmt.pix_mp.num_planes;
	printf("Number of planes: %d\n", num_planes);
	for(int i = 0; i < num_planes; i++)
		printf("plane[%d].sizeimage = %u\n", i,
			fmt.fmt.pix_mp.plane_fmt[i].sizeimage);
}

/* require buffers for the driver and return the number of allocated bufs */
int v4l2_reqbuf(int fd, struct context ctx){
	struct v4l2_requestbuffers req;

	CLEAR(req);
	req.count = ctx.num_buffers;
	req.type = ctx.buf_type;
	req.memory = ctx.mem_type;
	xioctl(fd, VIDIOC_REQBUFS, &req);

	if (req.count < ctx.num_buffers)
		printf("Warning: couldn't allocate all requested buffers");

	return req.count;
}

/* query and get info of a buffer by the index */
void v4l2_querybuf(int fd, struct context ctx, int index, struct v4l2_plane *planes, struct v4l2_buffer *buf){

	CLEAR(*buf);
	CLEAR(*planes);

	buf->type       = ctx.buf_type;
	buf->memory     = ctx.mem_type;
	buf->index      = index;
	buf->m.planes	= planes;
	buf->length	= ctx.num_planes;

	xioctl(fd, VIDIOC_QUERYBUF, buf);

#ifdef DBG_PRINT
	printf("buffer queried: %d\n", index);
	for(int i = 0; i < ctx.num_planes; i++)
		printf("plane[%d].length = %u\n", i,
			buf->m.planes[i].length);
#endif

}

/* enqueue buffer by it index */
void v4l2_qbuf(int fd, struct context ctx, int index, struct v4l2_plane *planes, struct v4l2_buffer *buf){

	CLEAR(*buf);
	CLEAR(*planes);

	buf->type = ctx.buf_type;
	buf->memory = ctx.mem_type;
	buf->m.planes = planes;
	buf->length = ctx.num_planes;
	buf->index = index;
	xioctl(fd, VIDIOC_QBUF, buf);

}

/* dequeue the first avalible buffer and return it index */
int v4l2_dqbuf(int fd, struct context ctx, struct v4l2_plane *planes,
		struct v4l2_buffer *buf){

	CLEAR(*buf);
	CLEAR(*planes);
	buf->type = ctx.buf_type;
	buf->memory = ctx.mem_type;
	buf->m.planes = planes;
	buf->length = ctx.num_planes;
	xioctl(fd, VIDIOC_DQBUF, buf);

	return buf->index;
}

int main(int argc, char **argv)
{
	/* define context variables for this kind of capture */
	struct context			ctx;
	CLEAR(ctx);
	ctx.pix_format 	= V4L2_PIX_FMT_YVU420M;
	ctx.buf_type    = V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE;
	ctx.mem_type	= V4L2_MEMORY_MMAP;
	ctx.num_planes	= NUM_PLANES;
	ctx.num_buffers	= 5;
	ctx.img_height	= 360;
	ctx.img_width	= 640;
	ctx.size_image	= ctx.img_height * ctx.img_width;

	struct v4l2_buffer              buf;
	struct v4l2_plane		planes[ctx.num_planes];
	enum v4l2_buf_type              type;
	fd_set                          fds;
	struct timeval                  tv;
	int                             r, fd = -1;
	unsigned int                    i, j;
	char                            *dev_name = "/dev/video0";
	char                            out_name[256];
	FILE                            *fout;
	struct buffer                   *buffers;

	/* Opening the device */
	fd = open(dev_name, O_RDWR | O_NONBLOCK, 0);
	if (fd < 0) {
		perror("Cannot open device");
		exit(EXIT_FAILURE);
	}
	
	/* Querying device capabilities */
	v4l2_querycap(fd);

	/* Setting image format */
	v4l2_s_fmt(fd, ctx);

#ifdef DBG_PRINT
	/* Getting image format */
	v4l2_g_fmt(fd, ctx);

	/* Trying to set a image format */
	v4l2_try_fmt(fd, ctx);
#endif

	/* Requiring buffers */
	ctx.num_buffers = v4l2_reqbuf(fd, ctx);

	/* Buffer memory allocation */
	buffers = calloc(ctx.num_planes * ctx.num_buffers, sizeof(*buffers));

	/* for each buffer, allocate and set the planes */
	for (i = 0; i < ctx.num_buffers; i++){
		/* query the requested buffers */
		v4l2_querybuf(fd, ctx, i, planes, &buf);

		/* map planes offsets to buffers pointers */
		for(j = 0; j < ctx.num_planes; j++){

			buffers[i].length[j] = buf.m.planes[j].length;
			buffers[i].start[j] = mmap(NULL, buffers[i].length[j],
			PROT_READ | PROT_WRITE, MAP_SHARED,
			fd, buf.m.planes[j].m.mem_offset);

			if (MAP_FAILED == buffers[i].start[j]) {
				perror("mmap");
				exit(EXIT_FAILURE);
			}
		}
	}

	/* enqueue the buffers */
	for (i = 0; i < ctx.num_buffers; i++)
		v4l2_qbuf(fd, ctx, i, planes, &buf);

	/* start the streaming */
	type = ctx.buf_type;
	xioctl(fd, VIDIOC_STREAMON, &type);

	/* dequeue the buffers and capture the frames info */
	// for now it's only show one frame
	for (i = 0; i < 1; i++) {
		/* timestamp */
		do {
			FD_ZERO(&fds);
			FD_SET(fd, &fds);

			/* Timeout. */
			tv.tv_sec = 2;
			tv.tv_usec = 0;

			r = select(fd + 1, &fds, NULL, NULL, &tv);
		} while ((r == -1 && (errno = EINTR)));

		if (r == -1) {
			perror("select");
			return errno;
		}
	
		/* create the output file */
		sprintf(out_name, "out%03d.yuv", i);
		fout = fopen(out_name, "w");

		if (!fout) {
			perror("Cannot open image");
			exit(EXIT_FAILURE);
		}

		/* dequeue buffer */
		int index = v4l2_dqbuf(fd, ctx, planes, &buf);

		/* query the dequeued buffer */
		v4l2_querybuf(fd, ctx, index, planes, &buf);

		/* write each plane in the file */
		for(j = 0; j < ctx.num_planes; j++){
#ifdef DBG_PRINT
			printf("bytesused[%d]: %d\n", j, buf.m.planes[j].bytesused);
#endif

			fwrite(buffers[buf.index].start[j], 
				buf.m.planes[j].bytesused, 1, fout);
		}
	
		/* reinsert the buffer on queue and close the file */
		v4l2_qbuf(fd, ctx, i, planes, &buf);
		fclose(fout);
	}

	/* stop the streaming and free the memory */
	type = ctx.buf_type;
	xioctl(fd, VIDIOC_STREAMOFF, &type);
	for (i = 0; i < ctx.num_buffers; i++)
		for (j = 0; j < ctx.num_planes; j++)
			munmap(buffers[i].start[j], buffers[i].length[j]);

	close(fd);

	return 0;
}
